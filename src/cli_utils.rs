use libp2p::Swarm;
use std::collections::HashSet;

use log::{error, info};

use crate::read_local_tweets;
use crate::TweetBehaviour;
use crate::ListRequest;
use crate::TOPIC;
use crate::ListMode;

use crate::create_new_tweet;
use crate::publish_tweet;


pub async fn handle_list_peers(swarm: &mut Swarm<TweetBehaviour>) {
    info!("Discovered Peers:");
    let nodes = swarm.behaviour().mdns.discovered_nodes();
    let mut unique_peers = HashSet::new();
    for peer in nodes {
        unique_peers.insert(peer);
    }
    unique_peers.iter().for_each(|p| info!("{}", p));
}

pub async fn handle_list_tweets(cmd: &str, swarm: &mut Swarm<TweetBehaviour>) {
    let rest = cmd.strip_prefix("list tweets ");
    match rest {
        Some("all") => {
            let req = ListRequest {
                mode: ListMode::ALL,
            };
            let json = serde_json::to_string(&req).expect("can jsonify request");
            swarm
                .behaviour_mut()
                .floodsub
                .publish(TOPIC.clone(), json.as_bytes());
        }
        Some(tweets_peer_id) => {
            let req = ListRequest {
                mode: ListMode::One(tweets_peer_id.to_owned()),
            };
            let json = serde_json::to_string(&req).expect("can jsonify request");
            swarm
                .behaviour_mut()
                .floodsub
                .publish(TOPIC.clone(), json.as_bytes());
        }
        None => {
            match read_local_tweets().await {
                Ok(v) => {
                    info!("Local tweets ({})", v.len());
                    v.iter().for_each(|r| info!("{:?}", r));
                }
                Err(e) => error!("error fetching local tweets: {}", e),
            };
        }
    };
}

pub async fn handle_create_tweet(cmd: &str) {
    if let Some(rest) = cmd.strip_prefix("create r") {
        let elements: Vec<&str> = rest.split("|").collect();
        if elements.len() < 3 {
            info!("too few arguments - Format: username|content|hastags");
        } else {
            let username = elements.get(0).expect("username is there");
            let content = elements.get(1).expect("content is there");
            let hashtags = elements.get(2).expect("hashtags is there");
            if let Err(e) = create_new_tweet(username, content, hashtags).await {
                error!("error creating tweet: {}", e);
            };
        }
    }
}

pub async fn handle_publish_tweet(cmd: &str) {
    if let Some(rest) = cmd.strip_prefix("publish r") {
        match rest.trim().parse::<usize>() {
            Ok(id) => {
                if let Err(e) = publish_tweet(id).await {
                    info!("error publishing tweet with id {}, {}", id, e)
                } else {
                    info!("Published tweet with id: {}", id);
                }
            }
            Err(e) => error!("invalid id: {}, {}", rest.trim(), e),
        };
    }
}

