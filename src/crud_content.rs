use tokio::fs;
use tokio::sync::mpsc;

use log::{error, info};


use crate::STORAGE_FILE_PATH;
use crate::Tweet;
use crate::Tweets;
use crate::ListResponse;
use crate::ListMode;
use crate::Result;



pub fn respond_with_public_tweets(sender: mpsc::UnboundedSender<ListResponse>, receiver: String) {
    tokio::spawn(async move {
        match read_local_tweets().await {
            Ok(Tweets) => {
                let resp = ListResponse {
                    mode: ListMode::ALL,
                    receiver,
                    data: Tweets.into_iter().filter(|r| r.public).collect(),
                };
                if let Err(e) = sender.send(resp) {
                    error!("error sending response via channel, {}", e);
                }
            }
            Err(e) => error!("error fetching local tweets to answer ALL request, {}", e),
        }
    });
}

pub async fn create_new_tweet(username: &str, content: &str, hashtags: &str) -> Result<()> {
    let mut local_tweets = read_local_tweets().await?;
    let new_id = match local_tweets.iter().max_by_key(|r| r.id) {
        Some(v) => v.id + 1,
        None => 0,
    };
    local_tweets.push(Tweet {
        id: new_id,
        username: username.to_owned(),
        content: content.to_owned(),
        hashtags: hashtags.to_owned(),
        public: false,
    });
    write_local_tweets(&local_tweets).await?;

    info!("Created Tweet:");
    info!("Username: {}", username);
    info!("Content: {}", content);
    info!("Hashtags:: {}", hashtags);

    Ok(())
}

pub async fn publish_tweet(id: usize) -> Result<()> {
    let mut local_tweets = read_local_tweets().await?;
    local_tweets
        .iter_mut()
        .filter(|r| r.id == id)
        .for_each(|r| r.public = true);
    write_local_tweets(&local_tweets).await?;
    Ok(())
}

pub async fn read_local_tweets() -> Result<Tweets> {
     let content = fs::read(STORAGE_FILE_PATH).await?;
     let result = serde_json::from_slice(&content)?;
    Ok(result)
}

pub async fn write_local_tweets(Tweets: &Tweets) -> Result<()> {
    let json = serde_json::to_string(&Tweets)?;
    fs::write(STORAGE_FILE_PATH, &json).await?;
    Ok(())
}

