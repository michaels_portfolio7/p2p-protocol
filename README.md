# Peer to Peer Network and File Sharing

<p>A peer-to-peer (P2P) protocol written in Rust with a decentralized data .JSON storage and sharing based upon LibP2P.
</p>

[VIDEO PREVIEW DEMO](https://drive.google.com/file/d/1RZAIHkFrIP0Kgi4mD0741oLV_S_HZPhR/preview)


* `list peers` - list all peers
* `list tweets` - list user(local) tweets
* `list tweets all` - list all public tweets from all known peers
* `solcial user-tweets {peerId}` - list all public tweets from the given peer

* `create r Username|Content|Hashtags` - create a new recipe with the given data, the | are important as separators
